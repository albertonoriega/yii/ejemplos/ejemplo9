<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario1 $model */
/** @var ActiveForm $form */
?>
<div class="site-sumar">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'numero1') ?>
        <?= $form->field($model, 'numero2') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Sumar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
    
    

</div><!-- site-sumar -->

<div>
    El resultado es: <?= $resultado?>
</div>
<div>
    La suma es: <?= $suma?>
</div>
