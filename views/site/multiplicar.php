<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Formulario2 $model */
/** @var ActiveForm $form */
?>
<div class="site-multiplicar">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'numero1') ?>
        <?= $form->field($model, 'numero2') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Multiplicar', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-multiplicar -->

<div>
    El resultado es: <?= $resultado?>
    
</div>

<div>
    El producto es: <?= $producto?>
    
</div>