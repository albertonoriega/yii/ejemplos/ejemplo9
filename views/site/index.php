<?php

use yii\helpers\Html;
use yii\web\View;
/* @var $this View */

$this->title = 'Ejemplo 9';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent">
        <h1 class="display-4">Ejemplo 9 - Fomularios</h1>

        <p class="lead">Utilizacion de formularios en Yii con controles activos</p>

    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <h2>Ejercicio1</h2>
                <div>
                   
                    Formulario que te pide dos número y los suma 
                     <ul>
                    <li>Creamos un modelo llamado Formulario 1. Introducimos propiedades, attributeLabels y rules </li> 
                    <li>Creamos un método en el modelo para sumar los dos números </li>
                    <li>Creamos con gii un formulario con Form Generator. Esto nos crea una vista que la llamamos sumar y copiamos la action y la pegamos en el SiteController</li>
                    <li>En la action del controlador calculamos la suma</li>
                    <li>Le pasamos a la misma vista sumar el modelo y el resultado de la suma </li>
                    </ul>
                </div>
                    <p><?=Html::a("Ejercicio 1", // label
                        ["site/sumar"], // controlador/vista
                        ["class" => "btn btn-primary"]) // estilo visual del boton
                    ?></p>
            </div>
            <div class="col-lg-6">
                <h2>Ejercicio2</h2>
                <div>
                   
                    Formulario que te pide dos número y los multiplica 
                     <ul>
                    <li>Creamos un modelo llamado Formulario 2. Introducimos propiedades, attributeLabels y rules </li> 
                    <li>Creamos un método en el modelo para multiplicar los dos números </li>
                    <li>Creamos con gii un formulario con Form Generator. Esto nos crea una vista que la llamamos multiplicar y copiamos la action y la pegamos en el SiteController</li>
                    <li>En la action del controlador calculamos el producto</li>
                    <li>Le pasamos a la misma vista multiplicar el modelo y el resultado del producto </li>
                    </ul>
                </div>
                    <p><?=Html::a("Ejercicio 2", // label
                        ["site/multiplicar"], // controlador/vista
                        ["class" => "btn btn-primary"]) // estilo visual del boton
                    ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio3</h2>
                <div>
                   
                    Formulario que te pide un nombre y un apellido y te los muestra en un DetailView y los concatena 
                     <ul>
                    <li>Creamos un modelo llamado Formulario 3. Introducimos propiedades, attributeLabels y rules </li> 
                    <li>Creamos un método en el modelo para concatenar los textos </li>
                    <li>Creamos con gii un formulario con Form Generator. Esto nos crea una vista que la llamamos ejercicio3 y copiamos la action y la pegamos en el SiteController</li>
                    <li>En la action del controlador empleamos el metodo para concatenar</li>
                    <li>Le pasamos a la vista solucion3 el modelo y los textos concatenados </li>
                    <li>En la vista mostramos los datos en un DetailView y abajo mostramos los dos textos concatenados </li>
                    </ul>
                </div>
                    <p><?=Html::a("Ejercicio 3", // label
                        ["site/ejercicio3"], // controlador/vista
                        ["class" => "btn btn-primary"]) // estilo visual del boton
                    ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio4</h2>
                <div>
                   
                    Formulario que te pide tres numeros y los suma, resta, multiplica y divide
                     <ul>
                    <li>Creamos un modelo llamado Formulario 4. Introducimos propiedades, attributeLabels y rules </li> 
                    <li>Creamos un método para cada operación </li>
                    <li>Creamos con gii un formulario con Form Generator. Esto nos crea una vista que la llamamos ejercicio4 y copiamos la action y la pegamos en el SiteController</li>
                    <li>En la action del controlador asignamos una variable para calcular cada operacion</li>
                    <li>En la action del controlador tambien creamos un array con cada resultado de la operacion</li>
                    <li>Le pasamos a la vista solucion4 el modelo y los resultados de las operaciones </li>
                    <li>En la vista mostramos los datos en un DetailView y abajo mostramos los resultados de las operaciones </li>
                    </ul>
                </div>
                    <p><?=Html::a("Ejercicio 4", // label
                        ["site/ejercicio4"], // controlador/vista
                        ["class" => "btn btn-primary"]) // estilo visual del boton
                    ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio5</h2>
                <div>
                   
                    Formulario que te pide tres numeros y los suma, resta, multiplica y divide
                     <ul>
                    <li>Creamos un modelo llamado Formulario 5. Introducimos propiedades, attributeLabels y rules </li> 
                    <li>Creamos un método para cada operación </li>
                    <li>Creamos 4 propiedades privadas y los getter para dichas propiedades </li>
                    <li>Creamos con gii un formulario con Form Generator. Esto nos crea una vista que la llamamos ejercicio5 y copiamos la action y la pegamos en el SiteController</li>
                    <li>En la action del controlador le pasamos a la vista solucion5 el modelo</li>
                    <li>En la vista mostramos los datos en un DetailView, tanto los numeros introducidos, como los resultados de las operaciones</li>
                    </ul>
                </div>
                    <p><?=Html::a("Ejercicio 5", // label
                        ["site/ejercicio5"], // controlador/vista
                        ["class" => "btn btn-primary"]) // estilo visual del boton
                    ?></p>
            </div>
            
            <div class="col-lg-6">
                <h2>Ejercicio6</h2>
                <div>
                   
                    Formulario que te pide un texto y te indica yl número de cada vocal
                    <ul>
                    <li>Creamos un modelo llamado Formulario 6. Introducimos propiedades, attributeLabels y rules </li> 
                    <li>Creamos 5 propiedades privadas y los getter para dichas propiedades </li>
                    <li>Creamos con gii un formulario con Form Generator. Esto nos crea una vista que la llamamos ejercicio6 y copiamos la action y la pegamos en el SiteController</li>
                    <li>En la action del controlador le pasamos a la vista solucion6 el modelo</li>
                    <li>En la vista mostramos el texto en un DetailView y el n´mero de cada vocal a través de los getter del modelo</li>
                    </ul>
                </div>
                    <p><?=Html::a("Ejercicio 6", // label
                        ["site/ejercicio6"], // controlador/vista
                        ["class" => "btn btn-primary"]) // estilo visual del boton
                    ?></p>
            </div>
         </div>
    </div>
</div>