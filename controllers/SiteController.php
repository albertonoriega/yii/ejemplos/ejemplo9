<?php

namespace app\controllers;

use app\models\Formulario1;
use app\models\Formulario2;
use app\models\Formulario3;
use app\models\Formulario4;
use app\models\Formulario5;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use const YII_ENV_TEST;

class SiteController extends Controller {

    /**
     * {@inheritdoc}
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionSumar() {
        // Crear un modelo vacio del formulario
        // Crear un objeto de tipo Formulario1
        $model = new Formulario1();

        // Leer los datos del formulario en PHP
        //var_dump($_POST);
        // Para leer los datos del formulario desde el objeto request con el framework
        //var_dump(Yii::$app->request->post());
        // Inicializamos $resultado a 0
        $resultado = 0;
        $suma = 0;
        // Compruebo si ha pulsado el boton de sumar
        if (Yii::$app->request->post()) {
            // Relleno el modelo con todos los datos del formulario
            $model->load(Yii::$app->request->post());
            //$model->attributes = Yii::$app->request->post('Formulario1'); seria lo mismo que la linea de arriba
            // sumar los dos numeros
            $resultado = $model->numero1 + $model->numero2;
            // Llamamos al metodo sumar del modelo para sumar los dos numeros
            $suma = $model->Sumar();
        }
        // Carga la vista sumar. Le paso como argumento el objeto de tipo Formulario1
        // Le paso un modelo vacio
        // Render partial es para que te cargue la vista sin layout ni footer
        return $this->render('sumar', [
                    'model' => $model,
                    'resultado' => $resultado, // mandamos el resultado de la suma de los numeros en la misma vista que el formulario
                    'suma' => $suma, // mandamos la suma con el metodo suma a la misma vista
        ]);
    }

    public function actionMultiplicar() {
        $model = new Formulario2();

        $resultado = 0;
        $producto = 0;

        if (Yii::$app->request->post()) {

            $model->load(Yii::$app->request->post());

            $resultado = $model->numero1 * $model->numero2;
            // Llamamos al metodo sumar del modelo para sumar los dos numeros
            $producto = $model->Multiplicar();
        }
        return $this->render('multiplicar', [
                    'model' => $model,
                    'resultado' => $resultado,
                    'producto' => $producto,
        ]);
    }
    
    public function actionEjercicio3() {
        $model = new Formulario3();
        
        $resultado="";
        // Compruebo que hayan introducido los datos en el formulario
        if ($model->load(Yii::$app->request->post())) {
            // Compruebo que cumpla las reglas del modelo
            if ($model->validate()) {
                // Aquí es donde realizo la operación correspondiente
                // En este caso concatenar
                $resultado = $model->concatenar();
                // llamo a la vista para mostrar el contenido
                return $this->render('solucion3', [
                    'resultado'=> $resultado,
                    'model' => $model,
                ]);
            }
        }

        return $this->render('ejercicio3', [
                    'model' => $model,
        ]);
    }

    
    // Ejercicio4 tiene que sumar, multiplicar, dividir y restar los tres números
    
    public function actionEjercicio4() {
        $model = new Formulario4();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $suma=$model->sumar();
                $producto=$model->multiplicar();
                $resta=$model->restar();
                $cociente=$model->dividir();
                
                // en lugar de crear 4 variables, creamos un array asociativo
                $operaciones=[
                    "suma"=> $model->sumar(),
                    "resta"=> $model->restar(),
                    "producto" => $model->multiplicar(),
                    "cociente" => $model->dividir(),
                ];
                return $this->render('solucion4', [
                    'model'=>$model,
                    // Le pasamos la variable de cada operacion
                    'suma' => $suma,
                    'producto' => $producto,
                    'resta' => $resta,
                    'cociente' => $cociente,                    
                    'operaciones' => $operaciones // Le pasamos el array asociativo
                ]);
            }
        }

        return $this->render('ejercicio4', [
                    'model' => $model,
        ]);
    }
    
    // Lo mismo que le ejercicio4 pero creamos en el modelo propiedades privadas para que llame a los getter y le pasamos todas las operaciones con el modelo 
    public function actionEjercicio5() {
        $model = new Formulario5();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {

                return $this->render('solucion5', [
                    'model'=>$model,

                ]);
            }
        }

        return $this->render('ejercicio5', [
                    'model' => $model,
        ]);
    }
    
    // Introduzco nombre y me debe indicar el numero de a,e,i,o,u que tiene
    
    public function actionEjercicio6() {
        $model = new \app\models\Formulario6();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                return $this->render('solucion6', [
                            'model' => $model,
                ]);
            }
        }

        return $this->render('ejercicio6', [
                    'model' => $model,
        ]);
    }

}
