<?php


namespace app\models;

use yii\base\Model;


class Formulario6 extends Model {
    public ?string $nombre=null;
    
    private int $numeroA=0;
    private int $numeroE=0;
    private int $numeroI=0;
    private int $numeroO=0;
    private int $numeroU=0;
    private int $numeroVocales=0;
    
    public function attributeLabels(): array {
        return [
            'nombre' => 'Nombre',
            'numeroA' => 'Número de aes',
        ];
    }
    
    public function rules(): array {
        return [
            [['nombre'], 'required']
        ];
    }
    
    public function getNumeroA () {
        $a= substr_count(strtolower($this->nombre), "a");
        
        return $a;
    }
    
    public function getNumeroE () {
        $e= substr_count(strtolower($this->nombre), "e");
        return $e;
    }
    
    public function getNumeroI () {
        $i= substr_count(strtolower($this->nombre), "i");
        return $i;
    }
    
    public function getNumeroO () {
        $o= substr_count(strtolower($this->nombre), "o");
        return $o;
    }
    
    public function getNumeroU () {
        $u= substr_count(strtolower($this->nombre), "u");
        return $u;
    }
    public function getNumeroVocales () {
        $vocales= substr_count($this->nombre, 'a')+substr_count($this->nombre, 'e')+substr_count($this->nombre, 'i')+substr_count($this->nombre, 'o')+substr_count($this->nombre, 'u');
        
        return $vocales;
    }
    
}
    
    

