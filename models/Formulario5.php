<?php

namespace app\models;


class Formulario5 extends \yii\base\Model{
    // Propiedades para almacenar los numeros
    public ?int $numero1=null;
    public ?int $numero2=null;
    public ?int $numero3=null;
    // Propiedades para almacenar resultados
    // Al poner las propiedades privadas cuando llamas a la propiedad acude al getter que se llama igual a la propiedad
    private int $suma=0;
    private int $resta=0;
    private int $producto=0;
    private float $cociente=0;


    public function attributeLabels(): array {
        return [
            'numero1' => 'Número 1',
            'numero2' => 'Número 2',
            'numero3' => 'Número 3',
        ];
    }
    
    public function rules(): array {
        return [
            [['numero1','numero2','numero3'], 'integer'],
            [['numero1','numero2','numero3'], 'required'],
        ];
    }
    
    public function getSuma() {
        
        return $this->numero1+$this->numero2+$this->numero3;
    }
    
    public function getProducto() {
        
        return $this->numero1*$this->numero2*$this->numero3;
    }
    
    public function getResta() {
        
        return $this->numero1-$this->numero2-$this->numero3;
    }
    
    public function getCociente() {
        if ($this->numero2==0 || $this->numero3==0){
            return "No se puede dividir entre 0";
        }
        return $this->numero1/$this->numero2/$this->numero3;
    }
}
