<?php

namespace app\models;


class Formulario3 extends \yii\base\Model {
    public ?string $nombre=null;
    public ?string $apellidos=null;
    
    public function attributeLabels(): array {
        return [
            'nombre' => 'Nombre del cliente',
            'apellidos' => 'Apellidos del cliente',
        ];    
    }
    
    public function rules(): array {
        return [
            [['nombre','apellidos'],'required'],
        ];
    }
    
    
    public function concatenar(): string {
        
        return $this->nombre . " " . $this->apellidos;
        
    }
}
