<?php

namespace app\models;


class Formulario4 extends \yii\base\Model{
    // Propiedades para almacenar los numeros
    public ?int $numero1=null;
    public ?int $numero2=null;
    public ?int $numero3=null;
    

    public function attributeLabels(): array {
        return [
            'numero1' => 'Número 1',
            'numero2' => 'Número 2',
            'numero3' => 'Número 3',
        ];
    }
    
    public function rules(): array {
        return [
            [['numero1','numero2','numero3'], 'integer'],
            [['numero1','numero2','numero3'], 'required'],
        ];
    }
    
    public function sumar() {
        
        return $this->numero1+$this->numero2+$this->numero3;
    }
    
    public function multiplicar() {
        
        return $this->numero1*$this->numero2*$this->numero3;
    }
    
    public function restar() {
        
        return $this->numero1-$this->numero2-$this->numero3;
    }
    
    public function dividir() {
        if ($this->numero2==0 || $this->numero3==0){
            return "No se puede dividir entre 0";
        }
        return $this->numero1/$this->numero2/$this->numero3;
    }
}
