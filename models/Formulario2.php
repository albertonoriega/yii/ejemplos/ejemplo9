<?php



namespace app\models;

use yii\base\Model;


class Formulario2 extends Model {
    public ?int $numero1=null;
    public ?int $numero2=null;
    
    public function attributeLabels(): array {
        return [
             'numero1' => 'Introduce el primer número',
            'numero2' => 'Introduce el segundo número',
        ];
    }
    
    public function rules(): array {
        return [
            [['numero1','numero2'], 'integer'],
            [['numero1','numero2'], 'required'],
        ];
    }
    
    // metodo para multiplicar los dos numeros
    public function Multiplicar(): int {
        
        return $this->numero1*$this->numero2;
    }
}
